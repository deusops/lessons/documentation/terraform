# Terraform  
[Официальная документация](https://developer.hashicorp.com/terraform/docs)  
[Основы Terraform](https://habr.com/ru/companies/otus/articles/696694/)  
[Что такое модули Terraform и как они работают?](https://habr.com/ru/articles/553576/)  
[Как использовать циклы в Terraform](https://ru.linux-console.net/?p=3412)  
[Использование output](https://developer.hashicorp.com/terraform/language/values/outputs)  

# Провайдеры  
[Установка для Yandex](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart)  
[Гайд от cloud.ru](https://cloud.ru/ru/docs/terraform/ug/doc-contents.html)  